import cv2, gdal, ogr, os, osr,sys,re
import numpy as np
import colorsys, fiona
import shutil ,glob
from shutil import copyfile
import pandas as pd
import geopandas as gpd
from shapely.geometry import shape, mapping
import itertools
from shapely.ops import unary_union
from operator import itemgetter
from tkinter import filedialog
from tkinter.ttk import Combobox
from tkinter import *
import xml.etree.ElementTree as ET
from joblib import Parallel, delayed
import multiprocessing

class shape():
    def rasterize(img_path, colors, output_path):
        name=func.nameFile(img_path)
        for c in colors:
            img=cv2.imread(img_path)
            bgr = [c[3], c[2], c[1]]
            thresh = 1 
            minBGR = np.array([bgr[0] - thresh, bgr[1] - thresh, bgr[2] - thresh])
            maxBGR = np.array([bgr[0] + thresh, bgr[1] + thresh, bgr[2] + thresh])
            maskBGR = cv2.inRange(img,minBGR,maxBGR)
            cv2.imwrite(output_path+"\\"+name+'_mask'+str(c[0])+'.tif',maskBGR)
            #print(func.nameFile(img_path))
            copyfile(img_path[:img_path.rfind(".")]+'.tfw', output_path+"\\"+name+'_mask'+str(c[0])+'.tfw')
	
    def polygonize(img,shp_path):
        # mapping between gdal type and ogr field type
        type_mapping = {gdal.GDT_Byte: ogr.OFTInteger,
                        gdal.GDT_UInt16: ogr.OFTInteger,
                        gdal.GDT_Int16: ogr.OFTInteger,
                        gdal.GDT_UInt32: ogr.OFTInteger,
                        gdal.GDT_Int32: ogr.OFTInteger,
                        gdal.GDT_Float32: ogr.OFTReal,
                        gdal.GDT_Float64: ogr.OFTReal,
                        gdal.GDT_CInt16: ogr.OFTInteger,
                        gdal.GDT_CInt32: ogr.OFTInteger,
                        gdal.GDT_CFloat32: ogr.OFTReal,
                        gdal.GDT_CFloat64: ogr.OFTReal}

        ds = gdal.Open(img)
        prj = ds.GetProjection()
        srcband = ds.GetRasterBand(1)
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource(shp_path)
        srs = osr.SpatialReference(wkt=prj)
        dst_layername=func.nameFile(img)
        dst_layer = dst_ds.CreateLayer(dst_layername, srs=srs)

        # raster_field = ogr.FieldDefn('id', type_mapping[srcband.DataType])
        raster_field = ogr.FieldDefn('H_LEVELM', ogr.OFTString)
        dst_layer.CreateField(raster_field)
        gdal.Polygonize(srcband, srcband, dst_layer, 0, [], callback=None)
        del  img,ds, srcband, dst_ds, dst_layer
            
    def mergerSHP(path,new_path,name,CRS,file_mask,columns):   
        file_list=func.getFilesPerExtName(path,'.shp',file_mask,100)
        if file_list:#not empty
            gd=[]
            for f in file_list:
                g = gpd.read_file(f)
                gd.append(g)
                gdf = gpd.GeoDataFrame(pd.concat(gd))
                    
            #change decimal places
            if columns:
                schema = gpd.io.file.infer_schema(gdf)
                schema['properties']['MIN_HEIGHT'] = 'float:6.1'
                schema['properties']['MAX_HEIGHT'] = 'float:6.1'
                gdf.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile', schema=schema)
            else:       
                gdf.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile')
    
    def createDS(ds_name, ds_format, geom_type, srs, overwrite=False):
        drv = ogr.GetDriverByName(ds_format)
        if os.path.exists(ds_name) and overwrite is True:
            deleteDS(ds_name)
        ds = drv.CreateDataSource(ds_name)
        lyr_name = os.path.splitext(os.path.basename(ds_name))[0]
        lyr = ds.CreateLayer(lyr_name, srs, geom_type)
        return ds, lyr
	
    def dissolve(input_shape, multipoly=False, overwrite=False):
        output_shape=func.addSuffix(input_shape,'_dissolve')
        ds = ogr.Open(input_shape)
        lyr = ds.GetLayer()
        out_ds, out_lyr = shape.createDS(output_shape, ds.GetDriver().GetName(), lyr.GetGeomType(), lyr.GetSpatialRef(), overwrite)
        defn = out_lyr.GetLayerDefn()
        multi = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat in lyr:
            if feat.geometry():
                feat.geometry().CloseRings() # this copies the first point to the end
                wkt = feat.geometry().ExportToWkt()
                multi.AddGeometryDirectly(ogr.CreateGeometryFromWkt(wkt))
        union = multi.UnionCascaded()
        if multipoly is False:
            for geom in union:
                poly = ogr.CreateGeometryFromWkb(geom.ExportToWkb())
                feat = ogr.Feature(defn)
                feat.SetGeometry(poly)
                out_lyr.CreateFeature(feat)
        else:
            out_feat = ogr.Feature(defn)
            out_feat.SetGeometry(union)
            out_lyr.CreateFeature(out_feat)
            out_ds.Destroy()
        ds.Destroy()
        return True
	
    def updateSHP(input_shape,column_name):
        output_shape=func.addSuffix(input_shape,'_update')
        gdf = gpd.read_file(input_shape)
        data = pd.DataFrame(gdf)
        if not {'H_LEVEL','MIN_HEIGHT', 'MAX_HEIGHT'}.issubset(data.columns):
            data['H_LEVEL']=column_name
            data['MIN_HEIGHT']=column_name
            data['MAX_HEIGHT']=column_name
            for i in range(0,len(data.index)):
                data.iat[i,2]=column_name
                data.iat[i,3]=func.splitNameDown(column_name)
                data.iat[i,4]=func.splitNameUpp(column_name)
        else:
            for i in range(0,len(data.index)):
                data.iat[i,2]=column_name
                data.iat[i,3]=func.splitNameDown(column_name)
                data.iat[i,4]=func.splitNameUpp(column_name)
        #save
        gdf1 = gpd.GeoDataFrame(data, geometry = gdf.geometry)
        schema = gpd.io.file.infer_schema(gdf1)
        #change decimal places
        schema['properties']['MIN_HEIGHT'] = 'float:6.1'
        schema['properties']['MAX_HEIGHT'] = 'float:6.1'
        gdf1.to_file(output_shape, driver='ESRI Shapefile',schema=schema) 

    def updateMerge(input_shape):
        output_shape=func.addSuffix(input_shape,'_update2')
        gdf = gpd.read_file(input_shape)
        data = pd.DataFrame(gdf)

        gdf1 = gpd.GeoDataFrame(data, crs=CRS, geometry = gdf.geometry)
        schema = gpd.io.file.infer_schema(gdf1)
        #change decimal places
        schema['properties']['MIN_HEIGHT'] = 'float:6.1'
        schema['properties']['MAX_HEIGHT'] = 'float:6.1'
        gdf1.to_file(output_shape, driver='ESRI Shapefile',schema=schema)
        
    def createPRJ(path,name,text):
        with open(path+"\\"+str(name)+".prj", "w") as file:
            file.write(text)

class XML:  
    def getSchemeXML(xml_path):
        tree = ET.parse(xml_path)
        root = tree.getroot()
        scheme=[]
        for elem in root:
            if elem.tag=='Theme':
                scheme.append(elem.attrib.get('name'))

        return scheme

    def getSchemeColors(xml_path,scheme):
        tree = ET.parse(xml_path)
        root = tree.getroot()
        color_scheme=[]
        for elem in root:
            if elem.attrib.get('name')==scheme:
                for var in elem:
                    name=var.attrib.get('height')
                    name=name[:name.find(".")+2]                
                    color_scheme.append([name,
                                    int(var.attrib.get('r')),
                                    int(var.attrib.get('g')),
                                    int(var.attrib.get('b'))])
        #name correction
        corr_scheme=[]
        for i in range(1,len(color_scheme)):
            if i==1 and color_scheme[0][0]=='-1.0':  #first row=-1 replace by 0
                name='0.0_'+color_scheme[1][0]+'m'             
            elif i==len(color_scheme)-1 and color_scheme[i][0]=='-1.0':   #last row=-1
                maxx=float(color_scheme[i-1][0])
                if maxx>=50:
                    name=color_scheme[i-1][0]+'_'+str(float(color_scheme[i-1][0])+20)+'m'
                else:
                    name=color_scheme[i-1][0]+"_50m"              
            else:
                name=color_scheme[i-1][0]+"_"+color_scheme[i][0]+"m"
            corr_scheme.append([name, color_scheme[i][1],color_scheme[i][2],color_scheme[i][3]])    
        return corr_scheme
    

class func:                                                                       
    def splitNameDown(text):
        countMinus=text.count('-')
        if countMinus==0:
            down_range=text[text.find('_')+1:text.rfind('_')]
        else:
            down_range=text[text.find('_-')+1:text.rfind('_')]
        return down_range
    
    def splitNameUpp(text):
        countMinus=text.count('-')
        if countMinus==0:
            upp_range=text[text.rfind('_')+1:text.rfind('m')]
        else:
            upp_range=text[text.rfind('_')+1:text.rfind('m')]
        return upp_range

    def getDirectories(path,extension):
        dir1=[name for name in os.listdir(path) if not '.' in name]
        directories=[]
        for d in dir1:
            if func.getFilesPerExtName(os.path.join(path,d),extension,'',0):
                if not d=='DifferenceMaps':
                    directories.append(d)
        return directories

    def getFilesPerExtName(input_folder,extensions,fname, size):
        file_list=[]
        for root, dirs, files in os.walk(input_folder):
            for name in files:
                filename, file_extension = os.path.splitext(name)
                if file_extension in extensions and fname in filename and size<os.path.getsize(os.path.join(root, name)):
                    file_list.append(os.path.join(root, name))
        return file_list

    def getFilesPerName(input_folder,fname, size):
        file_list=[]
        for root, dirs, files in os.walk(input_folder):
            for name in files:
                filename, file_extension = os.path.splitext(name)
                if fname in filename and size<os.path.getsize(os.path.join(root, name)):
                    file_list.append(os.path.join(root, name))
        return file_list                                                                                  

    def addSuffix(path,suffix):
        file_extension = path[-4:]
        path=path.replace(file_extension,suffix+file_extension)
        return path
    
    def nameFile(path):
        return os.path.basename(path)[:os.path.basename(path).rfind(".")]
            
    def createTemp(in_path, counter):
        if not os.path.exists(os.path.join(in_path,f'_temp{counter}')): 
            os.mkdir(os.path.join(in_path,f'_temp{counter}'))
        else: #remove files if exists
            func.clearTemp(os.path.join(in_path,f'_temp{counter}'),'*')
        return os.path.join(in_path,f'_temp{counter}')
    
    def clearTemp(temp_path,files):
        files = glob.glob(os.path.join(temp_path+'/'+files))       
        for f in files:
            os.remove(f)

    def swedify(inp):
        try:
            return inp.decode('utf-8')
        except:
            return '(!Dec:) ' + str(inp)

    def pathExist(path):
        if os.path.isfile(path):
            return True

class App:
    def __init__(self,master,st=0):
        self.master=master
        master.title('TIFF2SHP')

        #output
        self.label_1 = Label(master, text="Output: ")
        self.label_1.grid(row=2)
        self.entry_1=Entry(master, width=50)
        self.entry_1.insert(END,'')
        self.entry_1.grid(row=2,column=1)

        self.label_2 = Label(master, text="")
        self.label_2.grid(row=4, column=1)
        self.label_3 = Label(master, text="")
        self.label_3.grid(row=1, column=1)


        self.button_1 = Button(master, text="Get folder path subfolders", command=self.getOutput, height=1,width=25)
        self.button_1.grid(row=2,column=3,sticky=E+W,columnspan=2)

        self.button_2 = Button(master, text="Get LaserConfig", command=self.getConfig, height=1,width=25)
        self.button_2.grid(row=0,column=3,sticky=E+W,columnspan=2)

        #run button
        self.button_3 = Button(master, text="Run", command=self.main, height=1,width=25)
        self.button_3.grid(row=5,column=4,sticky=E+W,columnspan=2)

        #exit button
        self.close_button = Button(master, text="Exit", command=self.Quit, height=1,width=5)
        self.close_button.grid(row=5,column=0)

        #checkboxmerge
        self.cbMerge=IntVar()
        self.cbMerge.set(1)
        self.Checkbox=Checkbutton(master,text="Merg into one file",variable=self.cbMerge).grid(row=3,column=1,sticky=W)

        #Info button
        self.info_button = Button(master, text="Info", command=self.info, height=1,width=5)
        self.info_button.grid(row=3,column=0)

        #laserconfig
        self.label_CS = Label(master, text="Color_Scheme: ")
        self.label_CS.grid(row=0)
        self.entry_CS=Entry(master, width=50)
        self.entry_CS=Combobox(master,values='',width=47)
        self.entry_CS.grid(row=0,column=1)
        self.getConfig()
 
    def Quit(self):
            root.quit()
            root.destroy()
            sys.exit()
            
    def entryUpdate(self,text):
        self.entry_1.delete(0,END)
        self.entry_1.insert(0,text)

    def msg(self, message,color):
        self.message=message
        self.master.update()
        self.label_2.config(fg=color)
        self.label_2['text']=message        
        
    def info(self):
        self.label_2['text']="Select folder with powerlines folders \nwhere are shapefiles to merge - \nexport from DPM with DifferenceMaps"

    def getConfig(self):
        if not os.path.isfile('laserortho_config.xml'):            
            config=filedialog.askopenfilename(initialdir=os.getcwd(),title='Please select a laserortho_config.xml',filetypes = (("xml file","*.xml"),("all files","*.*")))
        else:
            config=os.path.join(os.getcwd(),'laserortho_config.xml')

        self.entry_CS['values']=XML.getSchemeXML(config)
        self.entry_CS.current(0)
        self.label_3['text']=config   
            
    def getOutput(self):
        path =  filedialog.askdirectory(initialdir=os.getcwd(),title='Please select a directory')
        if path=="":
            self.msg("Please select path",'red')
            return
        self.entry_1.delete(0,END)
        self.entry_1.insert(0,path)
        return path

    def main2(self, d, colors, path, counter):
        self.colors=colors
        self.d=d
        self.path=path
        i=1


        #get colors from XML
        
        prefix='vegetation_'
        suffix='_diff_map'     
        CRS='PROJCS["ETRS89_UTM_zone_32N",GEOGCS["GCS_ETRS_1989",\
        DATUM["D_ETRS_1989",SPHEROID["GRS_1980",6378137,298.257222101]],\
        PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],\
        PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],\
        PARAMETER["central_meridian",9],PARAMETER["scale_factor",0.9996],\
        PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]'

        print("Processing folder: "+d)
    

        catalog=os.path.join(path,d)
        temp_path=func.createTemp(path, counter)
        oryg_files=func.getFilesPerExtName(catalog,['.tif','.tfw'],'',0)
        #backup data and remove swedish letters
        print("create backup")
        backup=os.path.join(temp_path,'_backup')

        if not os.path.exists(backup): 
            os.mkdir(backup)        

        for oryg in oryg_files:
            copyfile(oryg,os.path.join(backup, re.sub('[^a-zA-Z0-9-_*.]', '', os.path.basename(oryg))))   #copy without swedish letters       

        file_list=func.getFilesPerExtName(backup,'.tif','',0)
        
        for f in file_list:
            print("\t\tProcessing "+func.nameFile(f))
            name=func.nameFile(f)
            shape.rasterize(f,colors, temp_path)
            file_list_temp=func.getFilesPerExtName(temp_path,'.tif',name+'_mask',0)
            for ft in file_list_temp:
                print(ft)
                shape.polygonize(ft,temp_path)


        for c in colors:
            print(str(c)+'__merging')
            shape.mergerSHP(temp_path,temp_path,c[0]+'merg',CRS,c[0],'')
            
            if func.pathExist(os.path.join(temp_path,c[0]+'merg.shp')):
                print(str(c)+'__dissolving')
                shape.dissolve(os.path.join(temp_path,c[0]+'merg.shp'))    
                print(str(c)+'__updating')
                shape.updateSHP(os.path.join(temp_path,c[0]+'merg_dissolve.shp'),prefix+c[0])

        #checkbutton to merge into one file

        if self.cbMerge.get()==1:
            shape.mergerSHP(temp_path,catalog,d,CRS,'merg_dissolve_update','yes')
            shape.createPRJ(catalog,d,CRS)
        else:
            notmerge=func.getFilesPerName(temp_path,'merg_dissolve_update', 0)
            for k in notmerge:
                file_name, file_extension = os.path.splitext(os.path.basename(k))
                file_name=file_name.replace('merg_dissolve_update','')
                copyfile(k,os.path.join(catalog,file_name+file_extension))
                if file_extension=='.shp':
                    shape.createPRJ(catalog,file_name,CRS)              
        shutil.rmtree(temp_path)                

           

        print("Done")   
        self.msg("Done without errors",'green')

    def main(self):
        #get colors from XML
        #get path
        window=Tk()
        window.withdraw()
        window.title("Creare new folder and move files")

        path=self.entry_1.get()

        self.msg("Processing..",'black')
        #check if open folder with folders
        directories=func.getDirectories(path,'.tif')
        if not directories:
            self.msg("No folders with TIFFs to batch run",'black')

        colors=XML.getSchemeColors(self.label_3['text'],self.entry_CS.get())

        counter=1


         
        num_cores = multiprocessing.cpu_count()
             
        results = Parallel(n_jobs=num_cores)(delayed(self.main2)(d,colors, path, counter) for d in directories)


if __name__=="__main__":
    root=Tk()
    app=App(root)
    root.mainloop()
    
