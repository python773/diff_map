import cv2, gdal, ogr, os, osr
import numpy as np
import colorsys, fiona
import shutil ,glob
from shutil import copyfile
import pandas as pd
import geopandas as gpd
from shapely.geometry import shape, mapping
import itertools
from shapely.ops import unary_union
from operator import itemgetter


class shape():
    def rasterize(img_path, colors, output_path):
        name=func.nameFile(img_path)
        for c in colors:
            img=cv2.imread(img_path)
            bgr = [c[3], c[2], c[1]]
            thresh = 1 
            minBGR = np.array([bgr[0] - thresh, bgr[1] - thresh, bgr[2] - thresh])
            maxBGR = np.array([bgr[0] + thresh, bgr[1] + thresh, bgr[2] + thresh])
            maskBGR = cv2.inRange(img,minBGR,maxBGR)
            cv2.imwrite(output_path+"\\"+name+'_mask'+str(c[0])+'.tif',maskBGR)
            #print(func.nameFile(img_path))
            copyfile(img_path[:img_path.rfind(".")]+'.tfw', output_path+"\\"+name+'_mask'+str(c[0])+'.tfw')
	
    def polygonize(img,shp_path):
        # mapping between gdal type and ogr field type
        type_mapping = {gdal.GDT_Byte: ogr.OFTInteger,
                        gdal.GDT_UInt16: ogr.OFTInteger,
                        gdal.GDT_Int16: ogr.OFTInteger,
                        gdal.GDT_UInt32: ogr.OFTInteger,
                        gdal.GDT_Int32: ogr.OFTInteger,
                        gdal.GDT_Float32: ogr.OFTReal,
                        gdal.GDT_Float64: ogr.OFTReal,
                        gdal.GDT_CInt16: ogr.OFTInteger,
                        gdal.GDT_CInt32: ogr.OFTInteger,
                        gdal.GDT_CFloat32: ogr.OFTReal,
                        gdal.GDT_CFloat64: ogr.OFTReal}

        ds = gdal.Open(img)
        prj = ds.GetProjection()
        srcband = ds.GetRasterBand(1)
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource(shp_path)
        srs = osr.SpatialReference(wkt=prj)
        dst_layername=func.nameFile(img)
        dst_layer = dst_ds.CreateLayer(dst_layername, srs=srs)

        # raster_field = ogr.FieldDefn('id', type_mapping[srcband.DataType])
        raster_field = ogr.FieldDefn('H_LEVELM', ogr.OFTString)
        dst_layer.CreateField(raster_field)
        gdal.Polygonize(srcband, srcband, dst_layer, 0, [], callback=None)
        del  img,ds, srcband, dst_ds, dst_layer
            
    def mergerSHP(path,new_path,name,CRS,file_mask,columns):   
        file_list=func.getFilesPerExtName(path,'.shp',file_mask)
        gd=[]
        for f in file_list:
            g = gpd.read_file(f)
            gd.append(g)
            gdf = gpd.GeoDataFrame(pd.concat(gd))
                
        #change decimal places
        if columns:
            schema = gpd.io.file.infer_schema(gdf)
            schema['properties']['MIN_HEIGHT'] = 'float:6.1'
            schema['properties']['MAX_HEIGHT'] = 'float:6.1'
            gdf.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile', schema=schema)
        else:       
            gdf.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile')
    
    def createDS(ds_name, ds_format, geom_type, srs, overwrite=False):
        drv = ogr.GetDriverByName(ds_format)
        if os.path.exists(ds_name) and overwrite is True:
            deleteDS(ds_name)
        ds = drv.CreateDataSource(ds_name)
        lyr_name = os.path.splitext(os.path.basename(ds_name))[0]
        lyr = ds.CreateLayer(lyr_name, srs, geom_type)
        return ds, lyr
	
    def dissolve(input_shape, multipoly=False, overwrite=False):
        output_shape=func.addSuffix(input_shape,'_dissolve')
        ds = ogr.Open(input_shape)
        lyr = ds.GetLayer()
        out_ds, out_lyr = shape.createDS(output_shape, ds.GetDriver().GetName(), lyr.GetGeomType(), lyr.GetSpatialRef(), overwrite)
        defn = out_lyr.GetLayerDefn()
        multi = ogr.Geometry(ogr.wkbMultiPolygon)
        for feat in lyr:
            if feat.geometry():
                feat.geometry().CloseRings() # this copies the first point to the end
                wkt = feat.geometry().ExportToWkt()
                multi.AddGeometryDirectly(ogr.CreateGeometryFromWkt(wkt))
        union = multi.UnionCascaded()
        if multipoly is False:
            for geom in union:
                poly = ogr.CreateGeometryFromWkb(geom.ExportToWkb())
                feat = ogr.Feature(defn)
                feat.SetGeometry(poly)
                out_lyr.CreateFeature(feat)
        else:
            out_feat = ogr.Feature(defn)
            out_feat.SetGeometry(union)
            out_lyr.CreateFeature(out_feat)
            out_ds.Destroy()
        ds.Destroy()
        return True
	
    def updateSHP(input_shape,column_name):
        output_shape=func.addSuffix(input_shape,'_update')
        gdf = gpd.read_file(input_shape)
        data = pd.DataFrame(gdf)
        if not {'H_LEVEL','MIN_HEIGHT', 'MAX_HEIGHT'}.issubset(data.columns):
            data['H_LEVEL']=column_name
            data['MIN_HEIGHT']=column_name
            data['MAX_HEIGHT']=column_name
            for i in range(0,len(data.index)):
                data.iat[i,2]=column_name
                data.iat[i,3]=func.splitNameDown(column_name)
                data.iat[i,4]=func.splitNameUpp(column_name)
        else:
            for i in range(0,len(data.index)):
                data.iat[i,2]=column_name
                data.iat[i,3]=func.splitNameDown(column_name)
                data.iat[i,4]=func.splitNameUpp(column_name)
        #save
        gdf1 = gpd.GeoDataFrame(data, crs=CRS, geometry = gdf.geometry)
        schema = gpd.io.file.infer_schema(gdf1)
        #change decimal places
        schema['properties']['MIN_HEIGHT'] = 'float:6.1'
        schema['properties']['MAX_HEIGHT'] = 'float:6.1'
        gdf1.to_file(output_shape, driver='ESRI Shapefile',schema=schema) 

    def updateMerge(input_shape):
        output_shape=func.addSuffix(input_shape,'_update2')
        gdf = gpd.read_file(input_shape)
        data = pd.DataFrame(gdf)

        gdf1 = gpd.GeoDataFrame(data, crs=CRS, geometry = gdf.geometry)
        schema = gpd.io.file.infer_schema(gdf1)
        #change decimal places
        schema['properties']['MIN_HEIGHT'] = 'float:6.1'
        schema['properties']['MAX_HEIGHT'] = 'float:6.1'
        gdf1.to_file(output_shape, driver='ESRI Shapefile',schema=schema)
        
    def createPRJ(path,name,text):
        with open(path+"\\"+str(name)+"_diff_map.prj", "w") as file:
            file.write(text)

class func:                                                                       
    def splitNameDown(text):
        countMinus=text.count('-')
        if countMinus==0:
            down_range=text[text.find('_')+1:text.rfind('_')]
        else:
            down_range=text[text.find('_-')+1:text.rfind('_')]
        return down_range
    
    def splitNameUpp(text):
        countMinus=text.count('-')
        if countMinus==0:
            upp_range=text[text.rfind('_')+1:text.rfind('m')]
        else:
            upp_range=text[text.rfind('_')+1:text.rfind('m')]
        return upp_range

    def getDirectories(path):
        dir1=[name for name in os.listdir(path) if not '.' in name]
        directories=[]
        for d in dir1:
            if Functions.getFilesPerExtName(os.path.join(path,d),'.shp',''):
                if not d=='DifferenceMaps':
                    directories.append(d)
        return directories

    def getFilesPerExtName(input_folder, extensions,fname):
        file_list=[]
        for root, dirs, files in os.walk(input_folder):
                for name in files:
                    filename, file_extension = os.path.splitext(name)
                    if file_extension in extensions and fname in filename:
                        file_list.append(os.path.join(root, name))
        return file_list
    
                                                                                  
    def addSuffix(path,suffix):
        file_extension = path[-4:]
        path=path.replace(file_extension,suffix+file_extension)
        return path
    
    def nameFile(path):
        return os.path.basename(path)[:os.path.basename(path).rfind(".")]
            
    def createTemp(path):
        if not os.path.exists('_temp'): 
            os.mkdir('_temp')
        else: #remove files if exists
            func.clearTemp(os.path.join(path,'_temp'),'*')
        return os.path.join(path,'_temp')
    
    def clearTemp(temp_path,files):
        files = glob.glob('_temp/'+files)
        for f in files:
            os.remove(f)
    

colors=[
['-15.0_-11.0m',255,0,0],
['-11.0_-6.0m',255,47,47],
['-6.0_-3.0m',255,74,74],
['-3.0_0.0m',255,128,128],
['0.0_1.0m',201,255,89],
['1.0_2.0m',0,253,0],
['2.0_3.0m',0,217,0],
['3.0_4.0m',0,191,0],
['4.0_5.0m',0,174,0],
['5.0_50m',0,128,0]]


path='d:\\MSZ\\__SCRIPTS, COMMANDS\\_Python - my scripts\\Diff_map\\'
prefix='vegetation_'

CRS='PROJCS["ETRS89_UTM_zone_32N",GEOGCS["GCS_ETRS_1989",\
DATUM["D_ETRS_1989",SPHEROID["GRS_1980",6378137,298.257222101]],\
PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],\
PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],\
PARAMETER["central_meridian",9],PARAMETER["scale_factor",0.9996],\
PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]'

temp_path=func.createTemp(path)

file_list=func.getFilesPerExtName(path,'.tif','DIFF_MAP')


for f in file_list:
    name=func.nameFile(f)
    shape.rasterize(f,colors, temp_path)
    file_list_temp=func.getFilesPerExtName(temp_path,'.tif',name+'_mask')
    for ft in file_list_temp:        
        shape.polygonize(ft,temp_path)
    shape.mergerSHP(temp_path,temp_path,func.nameFile(f),CRS,name+'_mask','')
    #func.clearTemp(temp_path,'*_mask*.tif')
    #func.clearTemp(temp_path,'*_mask*.tfw')


for c in colors:
    print(str(c)+'__merging')
    shape.mergerSHP(path,temp_path,c[0]+'merg',CRS,c[0],'')
    print(str(c)+'__dissolving')
    shape.dissolve(os.path.join(temp_path,c[0]+'merg.shp'))

    print(str(c)+'__updating')
    shape.updateSHP(os.path.join(temp_path,c[0]+'merg_dissolve.shp'),prefix+c[0])
print("merg")


shape.mergerSHP(temp_path,path,'merge',CRS,'merg_dissolve_update','yes')





