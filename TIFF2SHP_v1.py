import cv2, gdal, ogr, os, osr
import numpy as np
import colorsys, fiona
import shutil ,glob
from shutil import copyfile
import pandas as pd
import geopandas as gpd
from shapely.geometry import shape, mapping
import itertools
from shapely.ops import unary_union
from operator import itemgetter

def addSuffix(path,suffix):
    file_extension = path[-4:]
    path=path.replace(file_extension,suffix+file_extension)
    return path

def nameFile(path):
    return os.path.basename(path)[:os.path.basename(path).rfind(".")]
    
def createTemp(path):
    if not os.path.exists('_temp'): 
        os.mkdir('_temp')
    else: #remove files if exists
        clearTemp(os.path.join(path,'_temp'),'*')
    return os.path.join(path,'_temp')

def clearTemp(temp_path,files):
    files = glob.glob('_temp/'+files)
    for f in files:
        os.remove(f)

def rasterize(img_path, colors, output_path):
    name=nameFile(img_path)
    for c in colors:
        img=cv2.imread(img_path)
        bgr = [c[3], c[2], c[1]]
        thresh = 0 
        minBGR = np.array([bgr[0] - thresh, bgr[1] - thresh, bgr[2] - thresh])
        maxBGR = np.array([bgr[0] + thresh, bgr[1] + thresh, bgr[2] + thresh])
        maskBGR = cv2.inRange(img,minBGR,maxBGR)

        cv2.imwrite(output_path+"\\"+name+'_mask'+str(c[0])+'.tif',maskBGR)
        copyfile(img_path[:img_path.rfind(".")]+'.tfw', output_path+"\\"+name+'_mask'+str(c[0])+'.tfw')

def getFilesPerExtName(input_folder, extensions,fname):
    file_list=[]
    for root, dirs, files in os.walk(input_folder):
        for name in files:
            filename, file_extension = os.path.splitext(name)
            if file_extension in extensions and fname in filename:
                file_list.append(os.path.join(root, name))
    return file_list

def attName(path):
    first=nameFile(path)
    sec=first.replace('mask','')
    attNam='vegetation_'+sec
    return attNam

def polygonize(img,shp_path):
    # mapping between gdal type and ogr field type
    type_mapping = {gdal.GDT_Byte: ogr.OFTInteger,
                    gdal.GDT_UInt16: ogr.OFTInteger,
                    gdal.GDT_Int16: ogr.OFTInteger,
                    gdal.GDT_UInt32: ogr.OFTInteger,
                    gdal.GDT_Int32: ogr.OFTInteger,
                    gdal.GDT_Float32: ogr.OFTReal,
                    gdal.GDT_Float64: ogr.OFTReal,
                    gdal.GDT_CInt16: ogr.OFTInteger,
                    gdal.GDT_CInt32: ogr.OFTInteger,
                    gdal.GDT_CFloat32: ogr.OFTReal,
                    gdal.GDT_CFloat64: ogr.OFTReal}

    ds = gdal.Open(img)
    prj = ds.GetProjection()
    srcband = ds.GetRasterBand(1)
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource(shp_path)
    srs = osr.SpatialReference(wkt=prj)
    dst_layername=nameFile(img)
    dst_layer = dst_ds.CreateLayer(dst_layername, srs=srs)

    # raster_field = ogr.FieldDefn('id', type_mapping[srcband.DataType])
    raster_field = ogr.FieldDefn('H_LEVELM', ogr.OFTString)
    dst_layer.CreateField(raster_field)
    gdal.Polygonize(srcband, srcband, dst_layer, 0, [], callback=None)
    del  img,ds, srcband, dst_ds, dst_layer

def updateShp(img,shp_path):   
    #update shape value
    driver = ogr.GetDriverByName('ESRI Shapefile')
    fn = os.path.join(shp_path,nameFile(img)+'.shp')    
    dataSource = driver.Open(fn,1)
    layer = dataSource.GetLayer()
    feature = layer.GetNextFeature()   
    while feature:   
        feature.SetField("H_LEVELM", attName(img))
        layer.SetFeature(feature)
        feature = layer.GetNextFeature()
    dataSource.Destroy()

def mergerSHP(path,new_path,name,CRS,file_mask,columns):   
    file_list=getFilesPerExtName(path,'.shp',file_mask)
    gd=[]
    for f in file_list:
       g = gpd.read_file(f)
       gd.append(g)
    gdf = gpd.GeoDataFrame(pd.concat(gd))

    data = pd.DataFrame(gdf)
    if columns:
        for c in columns:
            if not {'H_LEVEL'}.issubset(data.columns):
                data['H_LEVEL']=data['H_LEVELM']
        gdf1 = gpd.GeoDataFrame(data, crs=CRS, geometry = gdf.geometry)
        schema = gpd.io.file.infer_schema(gdf1)    
        gdf1.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile')
    else: 
        gdf.to_file(os.path.join(new_path,name+".shp"), driver='ESRI Shapefile')  

def cleanGeom(input_shape):
    output_shape=addSuffix(input_shape,'_clean')
    gk500 = ogr.Open(input_shape, 1) # <====== 1 = update mode
    gk_lyr = gk500.GetLayer()

    for feature in gk_lyr:
        geom = feature.GetGeometryRef()
        if not geom.IsValid():
            feature.SetGeometry(geom.Buffer(0)) # <====== SetGeometry
            gk_lyr.SetFeature(feature) # <====== SetFeature
            assert feature.GetGeometryRef().IsValid() # Doesn't fail

    gk_lyr.ResetReading()
    assert all(feature.GetGeometryRef().IsValid() for feature in gk_lyr)  # Doesn't fail

def createDS(ds_name, ds_format, geom_type, srs, overwrite=False):
    drv = ogr.GetDriverByName(ds_format)
    if os.path.exists(ds_name) and overwrite is True:
        deleteDS(ds_name)
    ds = drv.CreateDataSource(ds_name)
    lyr_name = os.path.splitext(os.path.basename(ds_name))[0]
    lyr = ds.CreateLayer(lyr_name, srs, geom_type)
    return ds, lyr

def dissolve2(input_shape, multipoly=False, overwrite=False):
    output_shape=addSuffix(input_shape,'_dissolve')
    ds = ogr.Open(input_shape)
    lyr = ds.GetLayer()
    out_ds, out_lyr = createDS(output_shape, ds.GetDriver().GetName(), lyr.GetGeomType(), lyr.GetSpatialRef(), overwrite)
    defn = out_lyr.GetLayerDefn()
    multi = ogr.Geometry(ogr.wkbMultiPolygon)
    for feat in lyr:
        if feat.geometry():
            feat.geometry().CloseRings() # this copies the first point to the end
            wkt = feat.geometry().ExportToWkt()
            multi.AddGeometryDirectly(ogr.CreateGeometryFromWkt(wkt))
    union = multi.UnionCascaded()
    if multipoly is False:
        for geom in union:
            poly = ogr.CreateGeometryFromWkb(geom.ExportToWkb())
            feat = ogr.Feature(defn)
            feat.SetGeometry(poly)
            out_lyr.CreateFeature(feat)
    else:
        out_feat = ogr.Feature(defn)
        out_feat.SetGeometry(union)
        out_lyr.CreateFeature(out_feat)
        out_ds.Destroy()
    ds.Destroy()
    return True

def dissolve(input_shape):
    output_shape=addSuffix(input_shape,'_dissolve')
    inn = gpd.read_file(input_shape)
    inn=inn[['H_LEVELM','H_LEVEL','geometry']]
    inn2 = inn.dissolve(by='H_LEVELM',aggfunc='sum')
    inn2.to_file(os.path.join(output_shape), driver='ESRI Shapefile')     

def multi2single(input_shape):
    output_shape=addSuffix(input_shape,'_single')
    with fiona.open(input_shape) as source:
        # create the new file: the driver and crs are the same
        # for the schema the geometry type is "Polygon" instead
        output_schema = dict(source.schema)  # make an independant copy
        output_schema['geometry'] = "Polygon"

        with fiona.open(output_shape, 'w', 
                        driver=source.driver,
                        crs=source.crs,
                        schema=output_schema) as output:

            # read the input file
            for multi in source:

               # extract each Polygon feature
               for poly in shape(multi['geometry']):

                  # write the Polygon feature
                  output.write({
                      'properties': multi['properties'],
                      'geometry': mapping(poly)
                  })

colors=[
['-15.0_-11.0m',255,0,0],
['-11.0_-6.0m',255,47,47],
['-6.0_-3.0m',255,74,74],
['-3.0_0.0m',255,128,128],
['0.0_1.0m',201,255,89],
['1.0_2.0m',0,253,0],
['2.0_3.0m',0,217,0],
['3.0_4.0m',0,191,0],
['4.0_5.0m',0,174,0],
['5.0_50m',0,128,0]]


path='d:\\MSZ\\__SCRIPTS, COMMANDS\\_Python - my scripts\\Diff_map\\'
temp_path='d:\\MSZ\\__SCRIPTS, COMMANDS\\_Python - my scripts\\Diff_map\\_temp\\'
CRS='PROJCS["ETRS89_UTM_zone_32N",GEOGCS["GCS_ETRS_1989",\
DATUM["D_ETRS_1989",SPHEROID["GRS_1980",6378137,298.257222101]],\
PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],\
PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],\
PARAMETER["central_meridian",9],PARAMETER["scale_factor",0.9996],\
PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["Meter",1]]'

temp_path=createTemp(path)

file_list=getFilesPerExtName(path,'.tif','DIFF_MAP')
for f in file_list:
    name=nameFile(f)
    rasterize(f,colors, temp_path)
    file_list_temp=getFilesPerExtName(temp_path,'.tif',name+'_mask')
    for t in file_list_temp:        
        polygonize(t,temp_path)
        updateShp(t,temp_path)
    mergerSHP(temp_path,temp_path,nameFile(f),CRS,name+'_mask','')
    clearTemp(temp_path,'*_mask*.tif')
    clearTemp(temp_path,'*_mask*.tfw')


for c in colors:
    print(str(c)+'__merging')
    mergerSHP(path,temp_path,c[0]+'merg',CRS,c[0],'H_LEVEL')

    print(str(c)+'__dissolving')
    dissolve2(os.path.join(temp_path,c[0]+'merg.shp'))


print("merg")
mergerSHP(temp_path,path,'merge',CRS,'_dissolve','')
